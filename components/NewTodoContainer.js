import React from 'react';
import { connect } from 'react-redux';
import NewTodo from './NewTodo';
import store from './../state/store';
import { CHANGE_TEXT, changeText } from './../state/action_types';

const NewTodoContainer = React.createClass({

  changeTodoText: function(e) {
      store.dispatch(changeText(e.target.value))
  },

  addNewTodo: function(text) {
    this.props.addNewTodo(text);
    store.dispatch(changeText(''));
  },

  render: function() {
    return (
      <NewTodo text={this.props.todoText}
        changeTodoText={this.changeTodoText}
        addNewTodo={this.addNewTodo} />
    );
  }
});

const mapStateToProps = function(store) {
  return {
    todoText: store.newTodoText
  };
}

export default connect(mapStateToProps)(NewTodoContainer);

import React from 'react';
import { Label } from 'react-bootstrap';

export default React.createClass({
  // TODO: use this.props.addNewTodo.bind(null, document.getElementById().value)
  //       in onClick event handler if possible
  addNewTodo: function() {
    this.props.addNewTodo(document.getElementById('todo-text-box').value);
  },

  render: function() {
    return (
        <div>
        <h3><Label bsStyle='default'>Add a new Todo</Label></h3>
        <input type='text' id='todo-text-box' value={this.props.text} onChange={this.props.changeTodoText} />
        <button onClick={this.addNewTodo}>Add</button>
        </div>
    );
  }
});

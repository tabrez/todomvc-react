import React from 'react';
import { Label } from 'react-bootstrap';
import { connect } from 'react-redux';

import NewTodoContainer from './NewTodoContainer';
import TodoList from './TodoList';
import * as VisibilityFilters from './../VisibilityFilters';
import Footer from './Footer';
import store from './../state/store';
import { setVisibilityFilter,
         addNewTodo,
         togglePending,
         removeTodo,
         clearCompleted
       } from './../state/action_types';


// const todos_list = [{id: 0, text: 'Bring the milk', pending: true},
// {id: 1, text: 'Do laundry', pending: true},
// {id: 2, text: 'implement TodoMVC in React', pending: false}];

const TodoManager = React.createClass({
  // getInitialState: function() {
  //   return {
  //     todos: [],
  //     nextIndex: 3,
  //     filter: VisibilityFilters.SHOW_ALL
  //   };
  // },

  // componentDidMount: function() {
  //   this.setState(...this.state, { todos: todos_list });
  // },

  addNewTodo: function(todo) {
    // const newTodos = [...this.state.todos, {id: this.state.nextIndex, text: todo, pending: true}];
    // const newIndex = this.state.nextIndex + 1;
    // this.setState(...this.state, {todos: newTodos, nextIndex: newIndex});
    store.dispatch(addNewTodo(todo));
  },

  togglePending: function(todo) {
    // const newTodos = this.state.todos.map(
    //   (t) => {
    //     if(t.id == todo.id)
    //       t.pending = !t.pending;
    //     return t;
    //   })

    // this.setState(...this.state, {todos: newTodos});
    store.dispatch(togglePending(todo));
  },

  removeTodo: function(todo) {
    // const newTodos = this.state.todos.filter((t) => t.id != todo.id)
    // this.setState(...this.state, {todos: newTodos});
    store.dispatch(removeTodo(todo));
  },

  showActive: function() {
    store.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ACTIVE))
  },

  showCompleted: function() {
    store.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED));
  },

  showAll: function() {
    store.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ALL));
  },

  clearCompleted: function() {
    // const newTodos = this.state.todos.filter((todo) => todo.pending);
    // this.setState(...this.state, {todos: newTodos});
    store.dispatch(clearCompleted());
  },

  render: function() {
    return (
      <div style={{width:'640px'}}>
        <h1><Label bsStyle='default'>TodoMVC Application</Label></h1>

        <NewTodoContainer addNewTodo={this.addNewTodo} />

        <TodoList
          todos={this.props.todos}
          showFilter={this.props.showFilter}
          togglePending={this.togglePending}
          removeTodo={this.removeTodo} />

        <Footer
          itemsLeft={this.props.todos.filter((todo) => todo.pending).length}
          showFilter={this.props.showFilter}
          showAll={this.showAll}
          showActive={this.showActive}
          showCompleted={this.showCompleted}
          clearCompleted={this.clearCompleted}
          />
      </div>
    );
  }
});

const mapStateToProps = function(store) {
  return {
    newTodoText: store.newTodoState.newTodoText,
    showFilter: store.filterState.filter,
    todos: store.todoState.data
  };
}

export default connect(mapStateToProps)(TodoManager);

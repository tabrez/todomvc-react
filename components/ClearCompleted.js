import React from 'react';
import {Button} from 'react-bootstrap';

export default React.createClass({
  render: function() {
    return (
      <Button onClick={this.props.clearCompleted}>Clear All Completed</Button>
    );
  }
});

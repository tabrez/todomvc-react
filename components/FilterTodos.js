import React from 'react';
import {Button} from 'react-bootstrap';
import {ButtonGroup} from 'react-bootstrap';
import * as VisibilityFilters from './../VisibilityFilters'

export default React.createClass({
  render: function() {
    return (
      <ButtonGroup>
        <Button
          bsStyle={this.props.filter === VisibilityFilters.SHOW_ALL ? 'info' : 'default'}
          onClick={this.props.showAll}>All</Button>
        <Button
          bsStyle={this.props.filter === VisibilityFilters.SHOW_ACTIVE ? 'info' : 'default'}
          onClick={this.props.showActive}>Active</Button>
        <Button
          bsStyle={this.props.filter === VisibilityFilters.SHOW_COMPLETED ? 'info' : 'default'}
          onClick={this.props.showCompleted}>Completed</Button>
      </ButtonGroup>
    );
  }
});

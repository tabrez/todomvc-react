import React from 'react';

export default React.createClass({
  render: function() {
    return (
      <span>{this.props.itemsLeft} items left</span>
    );
  }
});

import React from 'react';
import * as VisibilityFilters from './../VisibilityFilters';
import { Button } from 'react-bootstrap';
import { Label, Table } from 'react-bootstrap';

export default React.createClass({
  filterTodos: function(filter, todos) {
    switch(filter) {
      case VisibilityFilters.SHOW_ALL:
        return todos;
      case VisibilityFilters.SHOW_ACTIVE:
        return todos.filter((todo) => todo.pending);
      case VisibilityFilters.SHOW_COMPLETED:
        return todos.filter((todo) => !todo.pending);
    }
  },

  generateTodoItem: function(todo) {
    return (
      <tr key={todo.id}>
        <td>
          <input type='checkbox'
            checked={!todo.pending} aria-lable='abc'
            onChange={this.props.togglePending.bind(null, todo)}/>
        </td>

        <td>
          {todo.text}
        </td>

        <td>
          <Button bsStyle='default' onClick={this.props.removeTodo.bind(null, todo)}>Remove</Button>
        </td>
      </tr>
    );
  },

  generateTodoList: function(filter, todos) {
    return this.filterTodos(filter, todos).map(this.generateTodoItem);
  },

  render: function() {
    return (
      <div>
        <h3><Label bsStyle='default'>Todo List</Label></h3>
        <Table bordered striped condensed hover>
          <tbody>
            {this.generateTodoList(this.props.showFilter, this.props.todos)}
          </tbody>
        </Table>
      </div>
    );
  }
});

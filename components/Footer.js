import React from 'react';
import ItemsLeft from './ItemsLeft';
import FilterTodos from './FilterTodos';
import ClearCompleted from './ClearCompleted';
import { Label, Well, InputGroup } from 'react-bootstrap';

export default React.createClass({
  render: function() {
    return (
      <div>
        <InputGroup>
          <Well bsSize='small'>
            <h4><Label bsStyle='default'><ItemsLeft itemsLeft={this.props.itemsLeft} /></Label></h4>

            <FilterTodos
              filter={this.props.showFilter}
              showAll={this.props.showAll}
              showActive={this.props.showActive}
              showCompleted={this.props.showCompleted} />

            <ClearCompleted clearCompleted={this.props.clearCompleted} />
          </Well>
        </InputGroup>
      </div>
    );
  }
});

import { createStore, combineReducers } from 'redux';
import { changeTextReducer, changeVisibilityFilterReducer, todoReducer } from './reducers';

const reducers = combineReducers({
  newTodoState: changeTextReducer,
  filterState: changeVisibilityFilterReducer,
  todoState: todoReducer
});
const store = createStore(reducers);

export default store;

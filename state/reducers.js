import { CHANGE_TEXT,
         SET_VISIBILITY_FILTER,
         ADD_NEW_TODO,
         REMOVE_TODO,
         TOGGLE_PENDING,
         CLEAR_COMPLETED
       } from './action_types';
import * as VisibilityFilters from './../VisibilityFilters';

const todosList =
[{id: 0, text: 'Bring the milk', pending: true},
{id: 1, text: 'Do laundry', pending: true},
{id: 2, text: 'implement TodoMVC in React', pending: false}];

// const initialState = {
//   newTodoText: { todoText : '' },
//   showFilter: VisibilityFilters.SHOW_ALL,
//   nextIndex: 3,
//   todos: todosList
// };

export const changeTextReducer = function(state = { todoText: '' }, action) {
  if(action.type === CHANGE_TEXT) {
    return { ...state, todoText: action.text };
  }
  return state;
};

export const changeVisibilityFilterReducer =
  function(state = { filter: VisibilityFilters.SHOW_ALL }, action) {
    if(action.type === SET_VISIBILITY_FILTER) {
      return {...state, filter: action.filter};
    }
    return state;
}

export const todoReducer = function(state = {data: todosList, nextIndex: 3}, action) {
  if(action.type == ADD_NEW_TODO) {
    const newTodos = [...state.data, {id: state.nextIndex,
                                            text: action.todo,
                                            pending: true}];
    const newIndex = state.nextIndex + 1;
    return {...state, data: newTodos, nextIndex: newIndex};
  }
  else if(action.type == REMOVE_TODO) {
    const newTodos = state.todos.filter((t) => t.id != action.todo.id)
    return {...state, data: newTodos};
   }
  else if(action.type == TOGGLE_PENDING) {
    const newTodos = state.data.map(
      (t) => {
        if(t.id == action.todo.id)
          t.pending = !t.pending;
        return t;
      })
    return {...state, data: newTodos};
  }
  else if(action.type == CLEAR_COMPLETED) {
    const newTodos = state.data.filter((todo) => todo.pending)
    return {...state, data: newTodos};
  }

  return state;
}

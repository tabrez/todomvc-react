// action types

export const CHANGE_TEXT = 'CHANGE_TEXT';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';
export const ADD_NEW_TODO = 'ADD_NEW_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const TOGGLE_PENDING = 'TOGGLE_PENDING';
export const CLEAR_COMPLETED = 'CLEAR_COMPLETED';

// action creators

export const changeText = function(text) {
  return { type: CHANGE_TEXT, text };
}

export const setVisibilityFilter = function(filter) {
  return { type: SET_VISIBILITY_FILTER, filter};
}

export const addNewTodo = function(todo) {
  return { type: ADD_NEW_TODO, todo };
}

export const removeTodo = function(todo) {
  return { type: REMOVE_TODO, todo };
}

export const togglePending = function(todo) {
  return { type: TOGGLE_PENDING, todo };
}

export const clearCompleted = function() {
  return { type: CLEAR_COMPLETED };
}
